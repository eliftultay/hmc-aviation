import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/storage";

const firebaseApp = firebase.initializeApp({
  apiKey: "your api key",
  authDomain: "your auth domain",
  projectId: "your project id on firebase",
  storageBucket: "your firebase storage bucket",
  messagingSenderId: "your sender id",
  appId: "your app's id",
  measurementId: "your measurement"
});


const storage = firebaseApp.storage();
const auth = firebaseApp.auth();
const database = firebaseApp.database();

export default firebaseApp
export { auth }
export { database };
export { storage };