import React, { Component } from "react";
import { Link } from "react-router-dom";
import { database, auth, storage } from "../../Database/Firebase";
import home from "../../Styles/Home/home-icon-pink.png";
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css'
import { css } from "@emotion/core";
import BounceLoader from "react-spinners/BounceLoader";
import InvalidAuth from "../Authentication/InvalidAuth"

class CreateStudent extends Component {

    constructor() {
        super()
        this.state = {
            name: '',
            surname: '',
            email: '',
            phone: '',
            birthdate: '',
            birthplace: '',
            passport: null,
            notes: '',
            passportKey: 'input_file',
            downloadURL: '',
            isLoading: false,
            isUserSignedIn: false
        }
    }

    isEmpty(str) {
        return (!str || 0 === str.length);
    }

    showPopup(popupMessage) {
        confirmAlert({
            title: 'CREATE STUDENT',
            message: popupMessage,
            customUI: ({ title, message, onClose }) =>
                < div className="w-80 px-10 py-8 bg-white font-family:Arial text-gray-600 text-center border rounded border-gray-600" >
                    <h3 className="w-full mb-1">{title}</h3>
                    <h4 className="w-full mb-3">{message}</h4>
                    <button
                        className="w-5/6 rounded text-white bg-lime-400 hover:bg-lime-600 focus:outline-none"
                        onClick={() => { onClose() }}
                    >
                        OK</button>
                </div >
        })
    };

    clearAllInput() {
        this.setState({
            name: '',
            surname: '',
            email: '',
            phone: '',
            birthdate: '',
            birthplace: '',
            passportKey: Date.now(),
            notes: '',
            downloadURL: ''
        });
    }

    async onSubmit(event) {
        event.preventDefault();

        if (this.isEmpty(this.state.name) || this.isEmpty(this.state.surname) ||
            this.isEmpty(this.state.email) || this.isEmpty(this.state.phone) ||
            this.isEmpty(this.state.birthdate) || this.isEmpty(this.state.birthplace)
            || this.state.passport == null || this.isEmpty(this.state.passport)) {
            this.showPopup("All the required fields must be filled (*)");
            return;
        }

        this.setState({ isLoading: true })

        // generate a unique key for each student
        let studentsRef = database.ref('students');
        let studentKey = studentsRef.push().key;

        // save passport image to storage
        let passportFile = this.state.passport;
        let studentStorageRef = storage.ref('students/' + studentKey);
        let isPassportSaved = true;
        await studentStorageRef.put(passportFile).then((task) => {
            if (!task) {
                this.showPopup("Couldn't save student passport image!");
                this.setState({ isLoading: false })
                isPassportSaved = false;
            }
        });

        if (isPassportSaved) {
            await studentStorageRef.getDownloadURL().then((downloadURL) => {
                this.state.downloadURL = downloadURL
            });

            let isStudentInfoSaved = true;
            await database.ref('students/' + studentKey).set({
                name: this.state.name.toLowerCase(),
                surname: this.state.surname.toLowerCase(),
                email: this.state.email.toLowerCase(),
                phone: this.state.phone.toLowerCase(),
                birthdate: this.state.birthdate.toLowerCase(),
                birthplace: this.state.birthplace.toLowerCase(),
                notes: this.state.notes.toLowerCase(),
                passport: this.state.downloadURL
            }, (error) => {
                if (error) {
                    isStudentInfoSaved = false;
                    this.showPopup("Couldn't add the student info!");
                    this.setState({ isLoading: false })
                }
            });
            if (isStudentInfoSaved) {
                this.showPopup('Student saved successfully.');
                this.setState({ isLoading: false })
                this.clearAllInput();
            }
        }
    };

    onChangeHandler = (event) => {
        const { name, value } = event.currentTarget;
        if (name === 'studentName') {
            this.setState({ name: value })
        }
        else if (name === 'studentSurname') {
            this.setState({ surname: value })
        }
        else if (name === 'studentEmail') {
            this.setState({ email: value })
        }
        else if (name === 'studentPhone') {
            this.setState({ phone: value })
        }
        else if (name === 'studentBirthdate') {
            this.setState({ birthdate: value })
        }
        else if (name === 'studentBirthplace') {
            this.setState({ birthplace: value })
        }
        else if (name === 'studentPassport') {
            this.setState({ passport: event.target.files[0] })
        }
        else if (name === 'studentNotes') {
            this.setState({ notes: value })
        }

    };

    componentDidMount() {
        auth.onAuthStateChanged((user) => {
            if (user) {
                this.setState({ isUserSignedIn: true });
            } else {
                this.setState({ isUserSignedIn: false });
            }
        });
    }

    render() {
        if (!this.state.isUserSignedIn) {
            return (
                <InvalidAuth></InvalidAuth>
            )
        }
        return (
            <div className="h-screen break-words px-20 pb-8">
                {/* Header */}
                <div className="h-1/6 flex font-sans text-3xl font-semibold tracking-wider text-left pt-8 text-pink-400">
                    <h2 className="w-3/6 pt-4">Create Student</h2>
                    <Link className="w-16 ml-auto" to="/home">
                        <img className="rounded-full" src={home} alt="pink" />
                    </Link>
                </div>

                {/* Elements */}
                <div className="h-5/6 font-sans placeholder-gray-200 bg-white">
                    <form className='h-full border-2 p-10 grid grid-rows-5 grid-cols-2 gap-x-4 gap-y-0 border-pink-100'>
                        <label className="block">
                            <span className="text-pink-400 text-xl">Name (*)</span>
                            <input
                                type="text"
                                name="studentName"
                                id="studentName"
                                className="mt-1 block w-full rounded-md bg-gray-100 border-transparent focus:border-gray-500 focus:bg-white focus:ring-0 capitalize"
                                value={this.state.name}
                                onChange={(event) => this.onChangeHandler(event)}
                            />
                        </label>
                        <label className="block">
                            <span className="text-pink-400 text-xl">Surname (*)</span>
                            <input
                                type="text"
                                name="studentSurname"
                                id="studentSurname"
                                className="mt-1 block w-full rounded-md bg-gray-100 border-transparent focus:border-gray-500 focus:bg-white focus:ring-0 capitalize"
                                value={this.state.surname}
                                onChange={(event) => this.onChangeHandler(event)}
                            />
                        </label>
                        <label className="block">
                            <span className="text-pink-400 text-xl">Email (*)</span>
                            <input
                                type="email"
                                name="studentEmail"
                                id="studentEmail"
                                className="mt-1 block w-full rounded-md bg-gray-100 border-transparent focus:border-gray-500 focus:bg-white focus:ring-0 lowercase"
                                placeholder="xxxxxxx@example.com"
                                value={this.state.email}
                                onChange={(event) => this.onChangeHandler(event)}
                            />
                        </label>
                        <label className="block">
                            <span className="text-pink-400 text-xl">Phone (*)</span>
                            <PhoneInput
                                country={'tr'}
                                onlyCountries={['tr', 'gr', 'de', 'us', 'es', 'uk', 'fr']}
                                value={this.state.phone}
                                onChange={(phone) => this.setState({ phone })}
                            />

                        </label>
                        <label className="block">
                            <span className="text-pink-400 text-xl">Birthdate (*)</span>
                            <input
                                type="date"
                                className="mt-1 block w-full rounded-md bg-gray-100 border-transparent focus:border-gray-500 focus:bg-white focus:ring-0 uppercase"
                                name="studentBirthdate"
                                id="studentBirthdate"
                                value={this.state.birthdate}
                                onChange={(event) => this.onChangeHandler(event)}
                            />
                        </label>
                        <label className="block">
                            <span className="text-pink-400 text-xl">Birthplace (*)</span>
                            <input
                                type="text"
                                name="studentBirthplace"
                                id="studentBirthplace"
                                className="mt-1 block w-full rounded-md bg-gray-100 border-transparent focus:border-gray-500 focus:bg-white focus:ring-0 capitalize"
                                value={this.state.birthplace}
                                onChange={(event) => this.onChangeHandler(event)}
                            />
                        </label>
                        <label className="block">
                            <span className="text-pink-400 text-xl">Passport Image (*)</span>
                            <input
                                type="file"
                                key={this.state.passportKey}
                                className="mt-1 block w-full rounded-md bg-gray-100 border-transparent focus:border-gray-500 focus:bg-white focus:ring-0 capitalize"
                                name="studentPassport"
                                id="studentPassport"
                                onChange={(event) => this.onChangeHandler(event)}
                            />
                        </label>
                        <label className="block">
                            <span className="text-pink-400 text-xl">Additional Notes</span>
                            <textarea
                                className="mt-1 block w-full rounded-md bg-gray-100 border-transparent focus:border-gray-500 focus:bg-white focus:ring-0 capitalize"
                                name="studentNotes"
                                id="studentNotes"
                                value={this.state.notes}
                                onChange={(event) => this.onChangeHandler(event)}
                            ></textarea>
                        </label>
                        <input
                            type="submit"
                            value="Add Student"
                            name="studentSubmit"
                            id="studentSubmit"
                            className="my-14 mx-auto h-12 block w-3/5 rounded-2xl bg-pink-300 hover:bg-pink-400 text-white outline-none capitalize"
                            onClick={(event) => {
                                this.onSubmit(event)
                            }}
                        />
                    </form>
                </div>
                <BounceLoader
                    color='#f472b6'
                    loading={this.state.isLoading}
                    size={150}
                    css={css`
                            display: block;
                            margin: 0 auto;
                            border-color: red;
                            position: absolute;
                            top: calc(50% - 75px);
                            left: calc(50% - 75px);
                            `}
                />
            </div>
        )
    }
}

export default CreateStudent