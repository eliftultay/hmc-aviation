import React, { Component } from "react";
import { Link } from "react-router-dom";
import { database, auth } from "../../Database/Firebase";
import home from "../../Styles/Home/home-icon-purple.png";
import StudentsList from './StudentsList';
import InvalidAuth from "../Authentication/InvalidAuth"

class Students extends Component {

    constructor() {
        super()
        this.state = {
            students: [],
            isUserSignedIn: false
        }
    }

    async fetchStudents() {
        var studentListRef = database.ref('/students');
        await studentListRef.on('value', (snapshot) => {
            var studentList = [];
            snapshot.forEach((childSnapshot) => {
                studentList.push({ 'key': childSnapshot.key, 'value': childSnapshot.val() });
            })
            this.setState({ students: studentList });
            console.log(this.state.students);
        });
    }

    componentDidMount() {
        this.fetchStudents();
        auth.onAuthStateChanged((user) => {
            if (user) {
                this.setState({ isUserSignedIn: true });
            } else {
                this.setState({ isUserSignedIn: false });
            }
        });
    }

    render() {
        if (!this.state.isUserSignedIn) {
            return (
                <InvalidAuth></InvalidAuth>
            )
        }
        return (
            <div className="h-screen break-words px-20 pb-8">
                <div className="h-1/6 flex font-sans text-3xl font-semibold tracking-wider text-left pt-8 text-lila border-b-2 border-purple-200">
                    <h2 className="w-1/2 pt-4">Students</h2>
                    <Link className="w-16 ml-auto" to="/home">
                        <img className="rounded-full" src={home} alt="purple" />
                    </Link>
                </div>
                <div className="grid grid-cols-3 gap-x-8 gap-y-0 uppercase">
                    {this.state.students.map((student, index) => <StudentsList key={student.key} student={student} index={index} />)}
                </div>
            </div>
        )
    }
}
export default Students