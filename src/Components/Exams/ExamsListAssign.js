import React, { Component } from "react";
import { Link } from "react-router-dom";
import Moment from 'react-moment'
import { database, auth } from "../../Database/Firebase";
import home from "../../Styles/Home/home-icon-black.png";
import back from "../../Styles/Icon/icon-back.png";
import ExamsListAssignStudent from './ExamsListAssignStudent'
import ExamsListEditStudent from "./ExamsListEditStudent";
import InvalidAuth from "../Authentication/InvalidAuth"

class ExamsListAssign extends Component {

    constructor() {
        super()
        this.state = {
            allStudents: [],
            selectedStudents: [],
            examTakers: [],
            selectedTakers: [],
            isUserSignedIn: false
        }
    }

    /*     union = (array1Pre, arrayNew) => {
            var result = [...array1Pre];
            for (var j = 0; j < arrayNew.length; j++) {
                var exists = false;
                for (var i = 0; i < result.length; i++) {
                    if (result[i].key === arrayNew[j].key) {
                        exists = true;
                    }
                }
                if (!exists) {
                    result.push(arrayNew[j]);
                }
            }
            return result;
        } */

    async fetchAllStudents() {
        var studentListRef = database.ref('/students');
        await studentListRef.on('value', (snapshot) => {
            let studentList = [];
            snapshot.forEach((childSnapshot) => {
                let exists = false;
                for (let index = 0; index < this.state.examTakers.length; index++) {
                    const taker = this.state.examTakers[index];
                    if (taker.key === childSnapshot.key) {
                        exists = true;
                        break;
                    }
                }
                if (exists === false) {
                    studentList.push({ 'key': childSnapshot.key, 'value': childSnapshot.val() });
                }
            })
            this.setState({ allStudents: studentList });
        });

    }

    async fetchExamTakers() {
        const key = this.props.location.state.examProps.key;
        const examRef = database.ref(`exam_dates/${key}/exam_takers`);
        await examRef.on("value", (snapshot) => {
            let takers = [];
            snapshot.forEach((childSnapshot) => {
                for (let index = 0; index < this.state.allStudents.length; index++) {
                    const student = this.state.allStudents[index];
                    if (student.key === childSnapshot.key) {
                        this.state.allStudents.splice(index, 1);
                    }
                }
                takers.push({
                    'key': childSnapshot.key,
                    'name': childSnapshot.val().name,
                    'surname': childSnapshot.val().surname,
                    'status': childSnapshot.val().status,
                    'certificated': childSnapshot.val().certificated
                });
            })
            this.setState({ examTakers: takers })
        })
        this.fetchAllStudents();
    }

    componentDidMount() {
        this.fetchExamTakers();
        auth.onAuthStateChanged((user) => {
            if (user) {
                this.setState({ isUserSignedIn: true });
            } else {
                this.setState({ isUserSignedIn: false });
            }
        });
    }

    render() {
        if (!this.state.isUserSignedIn) {
            return (
                <InvalidAuth></InvalidAuth>
            )
        }
        const exam = this.props.location.state.examProps;
        return (
            <div className='h-screen break-words px-20 pb-8'>
                {/* HEADER  */}
                <div className="h-1/5 flex font-sans text-2xl font-semibold tracking-wider text-left pt-8 text-gray-900 border-b-2 border-gray-500">
                    <h2 className="w-3/4 pt-4 uppercase">{exam.value.category.replace(/_/g, " ")} - {exam.value.type} - <Moment format="DD/MMMM/YYYY">{exam.value.date}</Moment></h2>
                    <div className="flex block ml-auto">
                        <Link to="/exams">
                            <img className="w-14 mx-8 rounded-full border-2 border-gray-500" src={back} alt="back" />
                        </Link>
                        <Link className="w-14" to="/home">
                            <img className="rounded-full" src={home} alt="yellow" />
                        </Link>
                    </div>
                </div>
                {/* BODY */}
                <div className="grid grid-cols-2 gap-x-2 h-3/5">
                    <div className="mt-10 px-6 overflow-y-auto border-r-2 border-gray-200 text-black">
                        <h3 className="w-full pb-4 font-semibold uppercase underline text-blue-800 text-xl">All Students</h3>
                        {this.state.allStudents
                            .map((student, index) => <ExamsListAssignStudent key={student.key}
                                student={student} exam={exam} index={index}
                            />)}
                    </div>
                    <div className="mt-10 px-6 overflow-y-auto text-black">
                        <h3 className="w-full pb-4 font-semibold uppercase underline text-blue-800 text-xl">Added Students</h3>
                        {this.state.examTakers
                            .map((taker, index) => <ExamsListEditStudent key={taker.key}
                                taker={taker} exam={exam} index={index}
                            />)}
                    </div>
                </div>
            </div>
        )
    }

}

export default ExamsListAssign