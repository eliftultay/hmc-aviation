import React, { Component } from "react";
import { Link } from "react-router-dom";
import { database, auth } from "../../Database/Firebase";
import home from "../../Styles/Home/home-icon-rose.png";
import ResultsList from "./ResultsList";
import InvalidAuth from "../Authentication/InvalidAuth"

class Results extends Component {

    constructor() {
        super()
        this.state = {
            exams: [],
            isUserSignedIn: false
        }
    }

    async fetchExams() {
        const examRef = database.ref('exam_dates/').orderByChild('date');
        await examRef.on("value", (snapshot) => {
            let exam = [];
            snapshot.forEach((childSnapshot) => {
                exam.push({ 'key': childSnapshot.key, 'value': childSnapshot.val() });
            })
            this.setState({ exams: exam })
        })
    }

    componentDidMount() {
        this.fetchExams();
        auth.onAuthStateChanged((user) => {
            if (user) {
                this.setState({ isUserSignedIn: true });
            } else {
                this.setState({ isUserSignedIn: false });
            }
        });
    }

    render() {
        if (!this.state.isUserSignedIn) {
            return (
                <InvalidAuth></InvalidAuth>
            )
        }
        return (
            <div className="h-screen break-words px-20 mb-20">
                <div className="h-1/5 flex font-sans text-3xl font-semibold tracking-wider text-left pt-8 text-rose-500 border-b-2 border-rose-300">
                    <h2 className="w-1/2 pt-4">Add Result</h2>
                    <Link className="w-16 ml-auto" to="/home">
                        <img className="rounded-full" src={home} alt="rose" />
                    </Link>
                </div>
                <div className="h-4/5 grid grid-cols-3 gap-x-8 gap-y-6">
                    {this.state.exams.map((exam, index) =>
                        <ResultsList
                            key={exam.key}
                            exam={exam}
                            index={index}
                        />)}
                </div>
            </div>
        )
    }
}

export default Results