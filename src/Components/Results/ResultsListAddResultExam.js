import React, { Component } from "react";
import { database } from "../../Database/Firebase";

class ResultsListAddResultExam extends Component {
    async onSubmit(event, examStatus) {
        event.preventDefault();
        let examKey = this.props.examKey;
        let studentKey = this.props.student.key;
        await database.ref('students/' + studentKey + '/taken_exams/' + examKey).update({
            status: examStatus
        }, (error) => {
            if (error) {
                console.log(error);
            }
        });
        await database.ref('exam_dates/' + examKey + '/exam_takers/' + studentKey).update({
            status: examStatus
        }, (error) => {
            if (error) {
                console.log(error);
            }
        });
    }

    render() {
        return (
            <div>
                {!this.props.student.value.certificated ?
                    <div className="w-full inline-flex grid grid-cols-6 gap-x-4 px-4 py-2 shadow-md">
                        <label className="col-span-4">
                            {this.props.student.value.name} {this.props.student.value.surname}
                        </label>
                        <button className="col-span-1 focus:outline-none rounded-full hover:bg-green-500 border-2"
                            onClick={(event) => { this.onSubmit(event, 'passed') }}>
                            P
                        </button>
                        <button className="col-span-1 focus:outline-none rounded-full hover:bg-red-500 border-2"
                            onClick={(event) => { this.onSubmit(event, 'failed') }}>
                            F
                        </button>
                    </div>
                    :
                    <div className="w-full inline-flex px-4 py-2 mb-2 shadow-md bg-gray-200">
                        <label>
                            {this.props.student.value.name} {this.props.student.value.surname}
                        </label>
                    </div>
                }

            </div>
        )
    }
}

export default ResultsListAddResultExam