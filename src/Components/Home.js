import React, { Component } from "react";
import { Link } from "react-router-dom";
import { auth } from "../Database/Firebase";
import InvalidAuth from "./Authentication/InvalidAuth"
import home from "../Styles/logo.png";

class Home extends Component {

  constructor() {
    super();
    this.state = {
      isUserSignedIn: false
    }
  }

  logOut(event) {
    auth.onAuthStateChanged(function (user) {
      if (user) {
        console.log(user)
      } else {
        console.log('signed out')
      }
    });
    event.preventDefault();
    auth.signOut().then((result) => {
      // Sign-out successful.
      console.log(result);
      this.props.history.push("/");
    }).catch((error) => {
      // An error happened.
      console.log(error.code + error.message)
    });
  }

  componentDidMount() {
    auth.onAuthStateChanged((user) => {
      if (user) {
        this.setState({ isUserSignedIn: true });
      } else {
        this.setState({ isUserSignedIn: false });
      }
    });
  }

  render() {
    if (!this.state.isUserSignedIn) {
      return (
        <InvalidAuth></InvalidAuth>
      )
    }
    return (
      <div className="h-screen w-screen min-h-screen">
        {/* Header */}
        <div className="h-1/6 w-screen flex">
          <Link to="/home" className="w-1/6 h-full mt-5">
            <img className="w-full h-full ml-20 flex-shrink" src={home} alt="home" />
          </Link>
          <button
            className="bg-magenta hover:bg-gold w-24 py-2 text-white rounded-2xl text-xl ml-auto mr-20 float-right max-h-12 mt-10 focus:outline-none"
            onClick={(event) => {
              this.logOut(event);
            }}
          >
            Log Out
            </button>
        </div>

        {/* Grid Button View  */}
        <div className="h-4/5 grid grid-rows-2 grid-cols-3 gap-8 py-10 mx-20 content-center place-content-evenly">
          <Link
            className="bg-pinky hover:bg-gray-300 w-full py-5 text-white rounded-2xl text-xl text-center py-24 "
            to="/create_student"
          >
            Create Student
            </Link>
          <Link
            className="bg-blue-400 hover:bg-gray-300 w-full py-5 text-white rounded-2xl text-xl text-center py-24"
            to="/create_exam_date"
          >
            Create Exam Dates
            </Link>
          <Link
            className="bg-lime hover:bg-gray-300 w-full py-5 text-white rounded-2xl text-xl text-center py-24"
            to="/certificated"
          >
            Certificated Students
            </Link>
          <Link
            className="bg-lila hover:bg-gray-300 w-full py-5 text-white rounded-2xl text-xl text-center py-24"
            to="/students"
          >
            Students
            </Link>
          <Link
            className="bg-gold hover:bg-gray-300 w-full py-5 text-white rounded-2xl text-xl text-center py-24"
            to="/exams"
          >
            Exams
            </Link>
          <Link
            className="bg-rose-400 hover:bg-gray-300 w-full py-5 text-white rounded-2xl text-xl text-center py-24"
            to="/results"
          >
            Add Result
            </Link>
        </div>
      </div>
    );
  }
}

export default Home;