module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'magenta': '#fa448c',
        'gold': '#fec859',
        'sea-blue': '#43b5a0',
        'sky-purple': '#491d88',
        'dark-purple': '#331a38',
        'lime': '#A3E635',
        'rose-50': '#FFF1F2',
        'rose-200': '#FECDD3',
        'rose-300': '#FDA4AF',
        'rose-400': '#FB7185',
        'rose-500': '#F43F5E',
        'rose-600': '#E11D48',
        'rose-700': '#BE123C',
        'rose-800': '#9F1239',
        'lime-50': '#F7FEE7',
        'lime-200': '#D9F99D',
        'lime-300': '#BEF264',
        'lime-400': '#A3E635',
        'lime-500': '#84CC16',
        'lime-600': '#65A30D',
        'lime-700': '#4D7C0F',
        'lime-800': '#3F6212',
        'orange': '#FB923C',
        'lila': '#C084FC',
        'pinky': '#F472B6'
      },
      backgroundImage: theme => ({
        'assign': "url('Icon/icon-add.png')",
        'back': "url('Icon/icon-back.png')",
        'cancel': "url('Icon/icon-cancel.png')",
        'certificate': "url('Icon/icon-certificate.png')",
        'delete': "url('Icon/icon-delete.png')",
        'edit': "url('Icon/icon-edit.png')",
        'minus': "url('Icon/icon-minus.png')",
        'plus': "url('Icon/icon-plus.png')",
        'print': "url('Icon/icon-print.png')",
        'result': "url('Icon/icon-result.png')",
        'tick': "url('Icon/icon-tick.png')",
        'visible': "url('Icon/icon-visible.png')",
        'invisible': "url('Icon/icon-invisible.png')",
        'certificated': "url('certificated.png')"
      })
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/forms')
  ]
}
